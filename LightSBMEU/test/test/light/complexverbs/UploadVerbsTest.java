package test.light.complexverbs;

import org.junit.Test;
import org.light.complexverb.AddUploadDomainField;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.easyui.EasyUIPositions;

public class UploadVerbsTest {
	public Domain getClockRecord(){		
		Domain domain = new Domain();
		domain.setPackageToken("org.peacewing");
		domain.setStandardName("ClockRecord");
		domain.addField("empid","long","");
		domain.addField("userid", "long","");
		domain.addField("timeStamp", "String","");
		domain.addField("description", "String","");
		domain.addField("photo","image","");
		domain.setDomainId(new Field("id", "long"));
		domain.setDomainName(new Field("clockRecordName","String"));
		domain.setActive(new Field("active", "boolean"));
		return domain;
	}
	
	@Test
	public void testUploadVerb()  throws Exception{		
		Domain domain = getClockRecord();
		domain.setDomainNamingSuffix("Entity");
		Field photo = domain.findFieldByFieldName("photo");
		
		AddUploadDomainField audf = new AddUploadDomainField(domain,photo);
		
		System.out.println("========================Controller===========");
		System.out.println(audf.generateControllerMethodString());
		System.out.println("========================ControllerWithSerial===========");
		System.out.println(audf.generateControllerMethodStringWithSerial());
		
		EasyUIPositions eups = (EasyUIPositions) audf;
		System.out.println("========================generateEasyUIJSActionStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSActionStringWithSerial());
		System.out.println("========================generateEasyUIJSButtonBlockStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSButtonBlockStringWithSerial());
	}
	
	
}
