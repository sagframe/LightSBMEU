package test.light.utils;

import org.junit.Test;
import org.light.exception.ValidateException;
import org.light.utils.StringUtil;

public class StringUtilTest {
	@Test
	public void testUnderLine(){
		try {
			System.out.println("==========testUnderLine=========");
			String retVal0 = StringUtil.changeTableColumtoDomainField("String_Util");
			System.out.println(retVal0);
//			String retVal1 = StringUtil.changeTableColumtoDomainField("String_Util_");
//			System.out.println(retVal1);
			String retVal2 = StringUtil.changeTableColumtoDomainField("_String_Util");
			System.out.println(retVal2);
			String retVal3 = StringUtil.changeTableColumtoDomainField("String__Util");
			System.out.println(retVal3);
		}catch (ValidateException ve) {
			System.out.println(ve.getMessage());
		}
	}

}