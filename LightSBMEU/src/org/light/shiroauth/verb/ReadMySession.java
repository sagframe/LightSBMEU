package org.light.shiroauth.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedStatementGenerator;
import org.light.generator.NamedStatementListGenerator;
import org.light.utils.InterVarUtil;
import org.light.utils.MybatisSqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ReadMySession extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}	

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;		
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {	
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception  {
		return null;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception  {
		return null;
	}
	
	public ReadMySession(){
		super();
		this.setLabel("读出会话");
	}
	
	public ReadMySession(Domain domain) throws ValidateException{
		super();
		this.domain = domain;
		this.denied = domain.isVerbDenied("ReadMySession");
		this.setVerbName("ReadMySession");
		this.setLabel("读出会话");
		if  (domain.getLanguage().equalsIgnoreCase("english"))  this.setLabel("ReadMySession");
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("readMySession");
			method.setReturnType(new Type("Map<String,Object>"));
			method.setThrowException(true);
			method.addAdditionalImport(this.domain.getPackageToken()+ "."+this.domain.getDomainSuffix()+"."+this.domain.getCapFirstDomainNameWithSuffix());
			method.addAdditionalImport(this.domain.getPackageToken()+"."+this.domain.getServiceSuffix()+"."+this.domain.getStandardName()+"Service");
			method.addSignature(new Signature(1,"httpSession","HttpSession"));
			method.addMetaData("RequestMapping(value = \"/readMySession\", method = RequestMethod.GET)");
	
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,2,"Map<String,Object> result = new TreeMap<String,Object>();"));
			//sList.add(new Statement(2000L,2,"session = httpSession;"));
			sList.add(new Statement(3000L,2,""+this.domain.getCapFirstDomainNameWithSuffix()+" "+this.domain.getLowerFirstDomainName()+" = ("+this.domain.getCapFirstDomainNameWithSuffix()+")httpSession.getAttribute(\""+this.domain.getLowerFirstDomainName()+"\");"));
			sList.add(new Statement(4000L,1,""));
			sList.add(new Statement(5000L,2,"result.put(\"success\",true);"));
			sList.add(new Statement(6000L,2,"result.put(\""+this.domain.getLowerFirstDomainName()+"\","+this.domain.getLowerFirstDomainName()+");"));
			sList.add(new Statement(7000L,2,"return result;"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}	

	@Override
	public String generateControllerMethodString()throws Exception {
		if (this.denied) return null;
		else {
			Method m = this.generateControllerMethod();
			return m.generateMethodString();
		}
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		if (this.denied) return null;
		else {
			Method m = this.generateControllerMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;	
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return null;
	}



	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		 return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;		
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		 return null;
	}
}
