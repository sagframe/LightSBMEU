package org.light.shiroauth.easyui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.complexverb.AddUploadDomainField;
import org.light.complexverb.DomainFieldVerb;
import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.DragonHideStatement;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyui.EasyUIPositions;
import org.light.easyuilayouts.EasyUIGridPageLayout;
import org.light.easyuilayouts.chiddgverb.Add;
import org.light.easyuilayouts.chiddgverb.Update;
import org.light.easyuilayouts.chiddgverb.UpdateUploadDomainField;
import org.light.generator.NamedS2SMJavascriptMethodGenerator;
import org.light.shiroauth.verb.AddUser;
import org.light.shiroauth.verb.ChangePasswordUser;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;
import org.light.verb.Activate;
import org.light.verb.ActivateAll;
import org.light.verb.Clone;
import org.light.verb.CloneAll;
import org.light.verb.Delete;
import org.light.verb.DeleteAll;
import org.light.verb.Export;
import org.light.verb.ExportPDF;
import org.light.verb.ExportWord;
import org.light.verb.FilterExcel;
import org.light.verb.FilterExcelFilterFields;
import org.light.verb.FilterPDF;
import org.light.verb.FilterPDFFilterFields;
import org.light.verb.FilterWord;
import org.light.verb.FilterWordFilterFields;
import org.light.verb.FindById;
import org.light.verb.FindByName;
import org.light.verb.ListActive;
import org.light.verb.ListAll;
import org.light.verb.ListAllByPage;
import org.light.verb.SearchByFieldsByPage;
import org.light.verb.SearchByName;
import org.light.verb.SoftDelete;
import org.light.verb.SoftDeleteAll;
import org.light.verb.Toggle;
import org.light.verb.ToggleOne;
import org.light.verb.View;
import org.light.verb.ViewWithDeniedFields;;

public class EasyUIUserPageLayout extends EasyUIGridPageLayout {
	protected Set<DomainFieldVerb> domainFieldVerbs = new TreeSet<>();
	protected Set<Verb> verbs = new TreeSet<>();

	public EasyUIUserPageLayout(Domain domain) throws Exception{
		super(domain);
		this.standardName = StringUtil.lowerFirst(domain.getPlural());
		Set<Verb> verbs = new TreeSet<Verb>();
		verbs.add(new ListAll(domain));
		verbs.add(new ListActive(domain));
		verbs.add(new Delete(domain));
		verbs.add(new FindById(domain));
		verbs.add(new FindByName(domain));
		verbs.add(new SearchByName(domain));
		verbs.add(new SoftDelete(domain));
		Update update = new Update(domain,this.getTechnicalStack(),this.getDbType());
		update.setDetailPrefix("");
		verbs.add(update);
		Add add = new Add(domain,this.getTechnicalStack(),this.getDbType());
		add.setDetailPrefix("");
		verbs.add(add);
		verbs.add(new ListAllByPage(domain));
		this.setVerbs(verbs);
		for (Field f:this.domain.getPlainFields()) {
			if (f.getFieldType().equalsIgnoreCase("image")) {
				domainFieldVerbs.add(new AddUploadDomainField(this.domain,f));
				domainFieldVerbs.add(new UpdateUploadDomainField(this.domain,f));
			}
		}
	}
	
	public Verb findVerb(String verbName){
		for (Verb v : this.verbs){
			if(v.getVerbName().equals(verbName))
				return v;
		}
		return null;
	}
	
	public List<Field> filterUserFields(List<Field> allFields){
		List<Field> results = new ArrayList<>();
		for (Field f:allFields) {
			String fieldFixName = f.getFixedName();
			if (!fieldFixName.equals("password")&&!fieldFixName.equals("salt")&&!fieldFixName.equals("loginFailure")) {
				results.add(f);
			}
		}
		return results;
	 }
	
	public List<Field> filterUserFieldsWithoutUserName(List<Field> allFields){
		List<Field> results = new ArrayList<>();
		for (Field f:allFields) {
			String fieldFixName = f.getFixedName();
			if (!fieldFixName.equals("password")&&!fieldFixName.equals("salt")&&!fieldFixName.equals("loginFailure")&&!fieldFixName.equals("userName")) {
				results.add(f);
			}
		}
		return results;
	 }
	
	@Override
	public StatementList generateLayoutStatements() {
		try {
			List<Writeable> sList =  new ArrayList<Writeable>();
			long serial = 0L;
			if  (this.domain.getLanguage().equalsIgnoreCase("english")){
				sList.add(new Statement(serial+8000L,0,"<div title=\"Search Panel\" class=\"easyui-panel\" style=\"width:1320px;height:200px\">"));
			}else {
				sList.add(new Statement(serial+8000L,0,"<div title=\"搜索面板\" class=\"easyui-panel\" style=\"width:1320px;height:200px\">"));					
			}
			sList.add(new Statement(serial+9000L,0,"<form id=\"ffsearch\" method=\"post\">"));
			sList.add(new Statement(serial+10000L,0,"<table cellpadding=\"5\">"));
			
			serial = serial + 11000L;
			List<Field> fields = new ArrayList<Field>();
			fields.addAll(this.domain.getSearchFields());
			fields.sort(new FieldSerialComparator());
			List<Field> userShowfields = filterUserFields(fields);
			Field f1,f2,f3;
			for (int i=0;i<userShowfields.size();i=i+3){
				f1 = userShowfields.get(i); 
				sList.add(new Statement(serial,0,"<tr>"));
				sList.add(new DragonHideStatement(serial+1000L,0,"<td>"+f1.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f1.getLowerFirstFieldName()+"' id='"+f1.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f1 instanceof Dropdown)));
				if (f1 instanceof Dropdown)
					sList.add(new DragonHideStatement(serial+1000L,0,"<td>"+f1.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f1.getLowerFirstFieldName()+"' id='"+f1.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f1).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f1).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f1).getTarget().getLowerFirstDomainName()+((Dropdown)f1).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f1).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td>",f1 instanceof Dropdown));
				if (i < userShowfields.size()-1) {
					f2 = userShowfields.get(i+1);
					sList.add(new DragonHideStatement(serial+2000L,0,"<td>"+f2.getText()+":</td><td><input class='easyui-textbox' type='text' name='"+f2.getLowerFirstFieldName()+"' id='"+f2.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f2 instanceof Dropdown)));					
					if (f2 instanceof Dropdown)
						sList.add(new DragonHideStatement(serial+2000L,0,"<td>"+f2.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f2.getLowerFirstFieldName()+"' id='"+f2.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f2).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f2).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f2).getTarget().getLowerFirstDomainName()+((Dropdown)f2).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f2).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td>",f2 instanceof Dropdown));
				}else{
					sList.add(new Statement(serial+2000L,0,"<td>&nbsp;</td>"));
				}
				if (i < userShowfields.size()-2) {
					f3 = userShowfields.get(i+2);
					sList.add(new DragonHideStatement(serial+3000L,0,"<td>"+f3.getText()+":</td><td><input class='easyui-textbox' type='text' name='"+f3.getLowerFirstFieldName()+"' id='"+f3.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td>",!(f3 instanceof Dropdown)));					
					if (f3 instanceof Dropdown)
						sList.add(new DragonHideStatement(serial+3000L,0,"<td>"+f3.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f3.getLowerFirstFieldName()+"' id='"+f3.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f3).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f3).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f3).getTarget().getLowerFirstDomainName()+((Dropdown)f3).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f3).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td>",f3 instanceof Dropdown));
				}else{
					sList.add(new Statement(serial+3000L,0,"<td>&nbsp;</td>"));
				}
				if (i==0) {
					sList.add(new Statement(serial+3100L,0,"<td>"));
					
					FilterExcel filterExcelDropdown = new FilterExcel(this.domain);
					FilterPDF filterPdfDropdown = new FilterPDF(this.domain);
					FilterWord filterWordDropdown = new FilterWord(this.domain);
					int dropdownindex = 2;
					if (!filterExcelDropdown.isDenied() || !filterPdfDropdown.isDenied()) {
						if  (this.domain.getLanguage().equalsIgnoreCase("english")){
							sList.add(new Statement(serial+3200L,2,"<select id=\"actionSelect\" class='easyui-select' style=\"	width:90px;height:28px\" onchange=\"toggleBtnShow(this.value)\">"));
							sList.add(new Statement(serial+3300L,3,"<option value=\"1\" selected>Search</option>"));
							if (!filterExcelDropdown.isDenied() ) sList.add(new Statement(serial+3400L,3,"<option value=\""+dropdownindex++ +"\">Filter Excel</option>"));
							if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+3500L,3,"<option value=\""+dropdownindex++ +"\">Filter PDF</option>"));
							if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+3550L,3,"<option value=\""+dropdownindex+"\">Filter Word</option>"));
							sList.add(new Statement(serial+3600L,2,"</select>"));
						}else {
							sList.add(new Statement(serial+3200L,2,"<select id=\"actionSelect\" class='easyui-select' style=\"	width:90px;height:28px\" onchange=\"toggleBtnShow(this.value)\">"));
							sList.add(new Statement(serial+3300L,3,"<option value=\"1\" selected>搜索</option>"));
							if (!filterExcelDropdown.isDenied() ) sList.add(new Statement(serial+3400L,3,"<option value=\""+dropdownindex++ +"\">Excel过滤</option>"));
							if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+3500L,3,"<option value=\""+dropdownindex++ +"\">PDF过滤</option>"));
							if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+3550L,3,"<option value=\""+dropdownindex+"\">Word过滤</option>"));
							sList.add(new Statement(serial+3600L,2,"</select>"));						
						}					
					}
					sList.add(new Statement(serial+3700L,2,"</td><td>"));
					sList.add(new Statement(serial+3800L,2,"<div id=\"button-bar\">"));
					if  (this.domain.getLanguage().equalsIgnoreCase("english")){
						sList.add(new Statement(serial+3900L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:block;width:110px;height:28px\" data-options=\"iconCls:'icon-search'\" onclick=\"search"+this.domain.getCapFirstPlural()+"ByFieldsByPage()\">Search</a>"));
						if (!filterExcelDropdown.isDenied()) sList.add(new Statement(serial+4000L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Excel()\">Filter Excel</a>"));
						if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+4100L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"PDF()\"\">Filter PDF</a>"));
						if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+4150L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Word()\"\">Filter Word</a>"));
					}else {
						sList.add(new Statement(serial+3900L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:block;width:110px;height:28px\" data-options=\"iconCls:'icon-search'\" onclick=\"search"+this.domain.getCapFirstPlural()+"ByFieldsByPage()\">搜索</a>"));
						if (!filterExcelDropdown.isDenied()) sList.add(new Statement(serial+4000L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Excel()\">Excel过滤</a>"));
						if (!filterPdfDropdown.isDenied() ) sList.add(new Statement(serial+4100L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"PDF()\"\">PDF过滤</a>"));
						if (!filterWordDropdown.isDenied() ) sList.add(new Statement(serial+4150L,3,"<a href=\"#\" class=\"easyui-linkbutton\" style=\"display:none;width:110px;height:28px\" data-options=\"iconCls:'icon-filter'\" onclick=\"filter"+this.domain.getCapFirstPlural()+"Word()\"\">Word过滤</a>"));
					}
					sList.add(new Statement(serial+4200L,2,"</div>"));
					sList.add(new Statement(serial+4300L,2,"</td><td>"));
					if  (this.domain.getLanguage().equalsIgnoreCase("english")){
						sList.add(new Statement(serial+4400L,3,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" style=\"width:80px\" data-options=\"iconCls:'icon-clear'\"  onclick=\"clearForm('ffsearch');toggleBtnShow(1);$('#actionSelect').val(1)\">Clear</a>"));
					}else {
						sList.add(new Statement(serial+4400L,3,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" style=\"width:80px\" data-options=\"iconCls:'icon-clear'\"  onclick=\"clearForm('ffsearch');toggleBtnShow(1);$('#actionSelect').val(1)\">清除</a>"));
					}
					sList.add(new Statement(serial+4500L,0,"</td>"));
				}else {
					sList.add(new Statement(serial+4600L,0,"<td></td><td colspan='3'></td>"));
				}
				sList.add(new Statement(serial+5000L,0,"</tr>"));
				serial += 6000L;
			}
			sList.add(new Statement(serial+1000L,0,"</table>"));
			sList.add(new Statement(serial+2000L,0,"</form>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));

			if  (this.domain.getLanguage().equalsIgnoreCase("english")){
				sList.add(new Statement(serial+4000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+" List\" style=\"width:1320px;height:400px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
			}else {
				sList.add(new Statement(serial+4000L,0,"<table id=\"dg\" class=\"easyui-datagrid\" title=\""+this.domain.getText()+"清单\" style=\"width:1320px;height:400px\" data-options=\"singleSelect:false,url:'../"+this.domain.getControllerPackagePrefix()+domain.getLowerFirstDomainName()+domain.getControllerNamingSuffix()+"/search"+domain.getCapFirstPlural()+"ByFieldsByPage',queryParams:params,method:'post',pagination:true,toolbar:toolbar\">"));
			}
			
			sList.add(new Statement(serial+5000L,0,"<thead>"));
			sList.add(new Statement(serial+6000L,0,"<tr>"));
			if (this.domain!=null&&this.domain.hasDomainId()) sList.add(new Statement(serial+7000L,0,"<th data-options=\"field:'"+this.domain.getDomainId().getLowerFirstFieldName()+"',checkbox:true\">"+this.domain.getDomainId().getText()+"</th>"));
			serial = serial + 8000L;
			List<Field> fields2 = new ArrayList<Field>();
			fields2.addAll(this.domain.getFieldsWithoutId());
			fields2.sort(new FieldSerialComparator());
			List<Field> userShowfields2 = filterUserFields(fields2);
			for (Field f: userShowfields2){
				if (f.getFieldType().equalsIgnoreCase("image")) {
					sList.add(new Statement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:140,formatter:show"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"Image\">"+f.getText()+"</th>"));
				}else {
					sList.add(new DragonHideStatement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80\">"+f.getText()+"</th>",!(f instanceof Dropdown)));
					if (f instanceof Dropdown)
						sList.add(new DragonHideStatement(serial,0,"<th data-options=\"field:'"+f.getLowerFirstFieldName()+"',width:80,formatter:translate"+((Dropdown)f).getTarget().getCapFirstDomainName()+"\">"+f.getText()+"</th>",f instanceof Dropdown));
				}
				serial+=1000L;
			}
			sList.add(new Statement(serial,0,"</tr>"));
			sList.add(new Statement(serial+1000L,0,"</thead>"));
			sList.add(new Statement(serial+2000L,0,"</table>"));
			sList.add(new Statement(serial+3000L,0,"</div>"));
			sList.add(new Statement(serial+4000L,0,""));
			Add addDialog = new Add(this.domain);
			if (this.domain.hasDomainId()&&this.domain.hasActiveField()&&!addDialog.isDenied()) {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+5000L,0,"<div class=\"easyui-window\" title=\"Add "+this.domain.getText()+"\" id=\"wadd"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}else {
					sList.add(new Statement(serial+5000L,0,"<div class=\"easyui-window\" title=\"新增"+this.domain.getText()+"\" id=\"wadd"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}
				sList.add(new Statement(serial+6000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
				sList.add(new Statement(serial+7000L,0,"<form id=\"ff\" method=\"post\">"));
				sList.add(new Statement(serial+8000L,0,"<table cellpadding=\"5\">"));
				sList.add(new Statement(serial+9000L,0,"<tr><td>"+this.domain.findFieldByFixedName("userName").getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+"' id='"+this.domain.findFieldByFixedName("userName").getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>"));
				sList.add(new Statement(serial+10000L,0,"<tr><td>"+this.domain.findFieldByFixedName("password").getText()+":</td><td><input  class='easyui-textbox' type='password' name='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' id='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>"));
				sList.add(new Statement(serial+11000L,0,"<tr><td>确认"+this.domain.findFieldByFixedName("password").getText()+":</td><td><input  class='easyui-textbox' type='password' name='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' id='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>"));
				
				serial = serial + 12000L;
				List<Field> fields3 = new ArrayList<Field>();
				fields3.addAll(this.domain.getFieldsWithoutIdAndActive());
				fields3.sort(new FieldSerialComparator());
				List<Field> userShowFields3 = filterUserFieldsWithoutUserName(fields3);
				
				for (Field f: userShowFields3){
					if (f.getFieldType().equalsIgnoreCase("image")) {
						sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><img name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' width='500px' src='../css/images/blank.jpg'><br>"));
						sList.add(new Statement(serial,0,"<input id=\"add"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"Fileupload\" type=\"file\" name=\"files[]\" data-url=\"../"+this.domain.getControllerPackagePrefix()+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/addUpload"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"\"><br></td></tr>"));
					}else {
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&!f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><textarea  class='easyui-textarea' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' data-options=\"required:false\" cols='20' rows='4'></textarea></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input class='easyui-radio' type='radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='false'/>False</td></tr>",(!(f instanceof Dropdown) && f.getFieldType().equalsIgnoreCase("boolean"))));
						if (f instanceof Dropdown)
							sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f).getTarget().getLowerFirstDomainName()+((Dropdown)f).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td></tr>", f instanceof Dropdown));
					}
					serial+=1000L;
				}
		        sList.add(new Statement(serial,0,"<tr><td>"+this.domain.getActive().getText()+":</td><td><input class='easyui-radio' type='radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='false'/>False</td></tr>"));
				sList.add(new Statement(serial+1000L,0,"</table>"));
				sList.add(new Statement(serial+2000L,0,"</form>"));
				sList.add(new Statement(serial+3000L,0,"<div style=\"text-align:center;padding:5px\">"));
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"add"+this.domain.getCapFirstDomainName()+"()\">Add</a>"));
					sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"clearForm('ff')\">Clear</a>"));
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wadd"+this.domain.getCapFirstDomainName()+"').window('close')\">Cancel</a>"));
				}else {
					sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"add"+this.domain.getCapFirstDomainName()+"()\">新增</a>"));
					sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"clearForm('ff')\">清除</a>"));
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wadd"+this.domain.getCapFirstDomainName()+"').window('close')\">取消</a>"));
				}
				sList.add(new Statement(serial+7000L,0,"</div>"));
				sList.add(new Statement(serial+8000L,0,"</div>"));
			}
			sList.add(new Statement(serial+9000L,0,""));

			Update updateDialog = new Update(this.domain);
			if (!updateDialog.isDenied()&&this.domain.hasDomainId()&&this.domain.hasActiveField()) {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+10000L,0,"<div class=\"easyui-window\" title=\"Edit "+this.domain.getText()+"\" id=\"wupdate"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}else {
					sList.add(new Statement(serial+10000L,0,"<div class=\"easyui-window\" title=\"编辑"+this.domain.getText()+"\" id=\"wupdate"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}
				sList.add(new Statement(serial+11000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
				sList.add(new Statement(serial+12000L,0,"<form id=\"ffedit\" method=\"post\">"));
				sList.add(new Statement(serial+13000L,0,"<input  type='hidden' name='"+this.domain.getDomainId().getLowerFirstFieldName()+"' id='"+this.domain.getDomainId().getLowerFirstFieldName()+"' value=''/>"));
				sList.add(new Statement(serial+14000L,0,"<table cellpadding=\"5\">"));
				serial = serial + 15000L;
				List<Field> fields4 = new ArrayList<Field>();
				fields4.addAll(this.domain.getFieldsWithoutIdAndActive());
				fields4.sort(new FieldSerialComparator());
				List<Field> userShowFields4 = filterUserFields(fields4);
				for (Field f: userShowFields4){
					if (f.getFieldType().equalsIgnoreCase("image")) {
						sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><img name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' width='500px' src='../css/images/blank.jpg'><br>"));
						sList.add(new Statement(serial,0,"<input id=\""+this.domain.getLowerFirstDomainName()+f.getCapFirstFieldName()+"Fileupload\" type=\"file\" name=\"files[]\" data-url=\"../"+this.domain.getControllerPackagePrefix()+this.domain.getLowerFirstDomainName()+this.domain.getControllerNamingSuffix()+"/addUpload"+this.domain.getCapFirstDomainName()+f.getCapFirstFieldName()+"\"><br></td></tr>"));
					}else {
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&!f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><textarea  class='easyui-textarea' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' data-options=\"required:false\" cols='20' rows='4'></textarea></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input class='easyui-radio' type='radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='false'/>False</td></tr>",(!(f instanceof Dropdown) && f.getFieldType().equalsIgnoreCase("boolean"))));
						if (f instanceof Dropdown)
							sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f).getTarget().getLowerFirstDomainName()+((Dropdown)f).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td></tr>", f instanceof Dropdown));
					}
					serial+=1000L;
				}
				if (this.domain!=null&& this.domain.hasActiveField()) sList.add(new Statement(serial,0,"<tr><td>"+this.domain.getActive().getText()+":</td><td><input class='easyui-radio' type='radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='false'/>False</td></tr>"));
				sList.add(new Statement(serial+1000L,0,"</table>"));
				sList.add(new Statement(serial+2000L,0,"</form>"));
				sList.add(new Statement(serial+3000L,0,"<div style=\"text-align:center;padding:5px\">"));
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"update"+this.domain.getCapFirstDomainName()+"()\">Edit</a>"));
					sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#ffedit').form('clear');\">Clear</a>"));
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wupdate"+this.domain.getCapFirstDomainName()+"').window('close')\">Cancel</a>"));
				}else {
					sList.add(new Statement(serial+4000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"update"+this.domain.getCapFirstDomainName()+"()\">编辑</a>"));
					sList.add(new Statement(serial+5000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#ffedit').form('clear');\">清除</a>"));
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wupdate"+this.domain.getCapFirstDomainName()+"').window('close')\">取消</a>"));
				}
				sList.add(new Statement(serial+7000L,0,"</div>"));
				sList.add(new Statement(serial+8000L,0,"</div>"));		
			}

			View viewDialog = new View(this.domain);
			if (!viewDialog.isDenied()) {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+10000L,0,"<div class=\"easyui-window\" title=\"View "+this.domain.getText()+"\" id=\"wview"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}else {
					sList.add(new Statement(serial+10000L,0,"<div class=\"easyui-window\" title=\"查看"+this.domain.getText()+"\" id=\"wview"+this.domain.getCapFirstDomainName()+"\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:800px;height:600px\">"));
				}
				sList.add(new Statement(serial+11000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
				sList.add(new Statement(serial+12000L,0,"<form id=\"ffview\" method=\"post\">"));
				sList.add(new Statement(serial+13000L,0,"<input  type='hidden' name='"+this.domain.getDomainId().getLowerFirstFieldName()+"' id='"+this.domain.getDomainId().getLowerFirstFieldName()+"' value=''/>"));
				sList.add(new Statement(serial+14000L,0,"<table cellpadding=\"5\">"));
				serial = serial + 15000L;
				List<Field> fields4 = new ArrayList<Field>();
				fields4.addAll(this.domain.getFieldsWithoutIdAndActive());
				fields4.sort(new FieldSerialComparator());
				List<Field> userShowFields4 = filterUserFields(fields4);
				for (Field f: userShowFields4){
					if (f.getFieldType().equalsIgnoreCase("image")) {
						sList.add(new Statement(serial,0,"<tr><td>"+f.getText()+":</td><td><img name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' width='500px' src='../css/images/blank.jpg'><br>"));
					}else {
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-textbox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false\"/></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&!f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><textarea  class='easyui-textarea' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' data-options=\"required:false\" cols='20' rows='4'></textarea></td></tr>",(!(f instanceof Dropdown) && !(f.getFieldType().equalsIgnoreCase("boolean"))&&f.isTextarea())));
						sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input class='easyui-radio' type='radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='false'/>False</td></tr>",(!(f instanceof Dropdown) && f.getFieldType().equalsIgnoreCase("boolean"))));
						if (f instanceof Dropdown)
							sList.add(new DragonHideStatement(serial,0,"<tr><td>"+f.getText()+":</td><td><input  class='easyui-combobox' type='text' name='"+f.getLowerFirstFieldName()+"' id='"+f.getLowerFirstFieldName()+"' value='' data-options=\"required:false,valueField:'"+((Dropdown)f).getTarget().getDomainId().getLowerFirstFieldName()+"',textField:'"+((Dropdown)f).getTarget().getDomainName().getLowerFirstFieldName()+"',method:'post',url:'../"+this.domain.getControllerPackagePrefix()+((Dropdown)f).getTarget().getLowerFirstDomainName()+((Dropdown)f).getTarget().getControllerNamingSuffix()+"/listActive"+((Dropdown)f).getTarget().getCapFirstPlural()+"',loadFilter:function(data){return data.rows}\"/></td></tr>", f instanceof Dropdown));
					}
					serial+=1000L;
				}
				if (this.domain!=null&& this.domain.hasActiveField()) sList.add(new Statement(serial,0,"<tr><td>"+this.domain.getActive().getText()+":</td><td><input class='easyui-radio' type='radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='true' checked='true'/>True<input type='radio' class='easyui-radio' name='"+this.domain.getActive().getLowerFirstFieldName()+"' id='"+this.domain.getActive().getLowerFirstFieldName()+"' value='false'/>False</td></tr>"));
				sList.add(new Statement(serial+1000L,0,"</table>"));
				sList.add(new Statement(serial+2000L,0,"</form>"));
				sList.add(new Statement(serial+3000L,0,"<div style=\"text-align:center;padding:5px\">"));
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wview"+this.domain.getCapFirstDomainName()+"').window('close')\">Close</a>"));
				}else {
					sList.add(new Statement(serial+6000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wview"+this.domain.getCapFirstDomainName()+"').window('close')\">关闭</a>"));
				}
				sList.add(new Statement(serial+7000L,0,"</div>"));
				sList.add(new Statement(serial+8000L,0,"</div>"));		
			}
			
			ChangePasswordUser changePassword = new ChangePasswordUser(this.domain);
			if (!changePassword.isDenied()) {
				if  (this.domain.getLanguage().equalsIgnoreCase("english")){
					sList.add(new Statement(serial+8500L,0,"<div class=\"easyui-window\" title=\"Change Password\" id=\"wchangePassword\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:600px;height:400px\">"));
					sList.add(new Statement(serial+9000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
					sList.add(new Statement(serial+10000L,0,"<form id=\"ffchangePassword\" method=\"post\">"));
					sList.add(new Statement(serial+11000L,0,"<input type=\"hidden\" id=\""+this.domain.getDomainName().getLowerFirstFieldName()+"\" name=\""+this.domain.getDomainName().getLowerFirstFieldName()+"\">"));
					sList.add(new Statement(serial+12000L,0,"<table cellpadding=\"5\">"));
					sList.add(new Statement(serial+13000L,2,"<tr><td>New Password:</td><td><input class='easyui-textbox' type='password' name='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' id='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' value='' data-options=\"required:true\"/></td></tr>"));
					sList.add(new Statement(serial+14000L,2,"<tr><td>Confirm New Password:</td><td><input class='easyui-textbox' type='password' name='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' id='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' value='' data-options=\"required:true\"/></td></tr>"));
					sList.add(new Statement(serial+15000L,0,"</table>"));
					sList.add(new Statement(serial+16000L,0,"</form>"));
					sList.add(new Statement(serial+17000L,0,"<div style=\"text-align:center;padding:5px\">"));
					sList.add(new Statement(serial+18000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"changePassword"+this.domain.getCapFirstDomainName()+"()\">Change Password</a>"));
					sList.add(new Statement(serial+19000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wchangePassword').window('close')\">Close</a>"));
					sList.add(new Statement(serial+20000L,0,"</div>"));
					sList.add(new Statement(serial+21000L,0,"</div>"));
				}else {
					sList.add(new Statement(serial+8500L,0,"<div class=\"easyui-window\" title=\"设置密码\" id=\"wchangePassword\" data-options=\"iconCls:'icon-save',modal:true,closed:true\" style=\"width:600px;height:400px\">"));
					sList.add(new Statement(serial+9000L,0,"<div style=\"padding:10px 60px 20px 60px\">"));
					sList.add(new Statement(serial+10000L,0,"<form id=\"ffchangePassword\" method=\"post\">"));
					sList.add(new Statement(serial+11000L,0,"<input type=\"hidden\" id=\""+this.domain.getDomainName().getLowerFirstFieldName()+"\" name=\""+this.domain.getDomainName().getLowerFirstFieldName()+"\">"));
					sList.add(new Statement(serial+12000L,0,"<table cellpadding=\"5\">"));
					sList.add(new Statement(serial+13000L,2,"<tr><td>新密码:</td><td><input class='easyui-textbox' type='password' name='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' id='"+this.domain.findFieldByFixedName("password").getLowerFirstFieldName()+"' value='' data-options=\"required:true\"/></td></tr>"));
					sList.add(new Statement(serial+14000L,2,"<tr><td>确认新密码:</td><td><input class='easyui-textbox' type='password' name='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' id='confirm"+this.domain.findFieldByFixedName("password").getCapFirstFieldName()+"' value='' data-options=\"required:true\"/></td></tr>"));
					sList.add(new Statement(serial+15000L,0,"</table>"));
					sList.add(new Statement(serial+16000L,0,"</form>"));
					sList.add(new Statement(serial+17000L,0,"<div style=\"text-align:center;padding:5px\">"));
					sList.add(new Statement(serial+18000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"changePassword"+this.domain.getCapFirstDomainName()+"()\">重设密码</a>"));
					sList.add(new Statement(serial+19000L,0,"<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" onclick=\"$('#wchangePassword').window('close')\">关闭</a>"));
					sList.add(new Statement(serial+20000L,0,"</div>"));
					
				}
			}
			StatementList mylist = WriteableUtil.merge(sList);
			return mylist;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public StatementList generateLayoutScriptStatements(){
		try {
			List<Writeable> sList =  new ArrayList<Writeable>();
			long serial = 0L;
			sList.add(new Statement(serial+24000L,0,"var params = {};"));			
			sList.add(new Statement(serial+25000L,0,"var pagesize = 10;"));
			sList.add(new Statement(serial+26000L,0,"var pagenum = 1;"));
			serial+=27000L;
			if (this.domainFieldVerbs!=null&&this.domainFieldVerbs.size()>0) {
				sList.add(new Statement(serial,0,"$(function () {"));	
				serial+=1000L;
				for (DomainFieldVerb dfv:this.domainFieldVerbs) {
					JavascriptBlock uploadBlock = dfv.generateEasyUIJSButtonBlock();
					StatementList  slupload = uploadBlock.getMethodStatementList();
					slupload.setSerial(serial);
					sList.add(slupload);
					serial+= 1000L;
				}
				sList.add(new Statement(serial+2000L,0,"});"));
			}
			sList.add(new Statement(serial+13000L,0,"var toolbar = ["));
			JavascriptBlock slViewJb = ((EasyUIPositions)new ViewWithDeniedFields(this.domain)).generateEasyUIJSButtonBlock();
			if (slViewJb != null) {
				StatementList  slView = slViewJb.getMethodStatementList();			
				slView.setSerial(serial+13010L);
				sList.add(slView);
				sList.add(new Statement(serial+13050L,0,","));
			}
			
			AddUser add = new AddUser(this.domain);
			add.setTechnicalStack(technicalStack);
			add.setDbType(this.dbType);
			//add.setDetailPrefix("");
			JavascriptBlock slAddJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)add).generateEasyUIJSButtonBlock():null;
			if (slAddJb != null) {
				StatementList  slAdd = slAddJb.getMethodStatementList();			
				slAdd.setSerial(serial+13100L);
				sList.add(slAdd);
				sList.add(new Statement(serial+13150L,0,","));
			}

			JavascriptBlock slChangePasswordJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ChangePasswordUser(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slChangePasswordJb != null) {
				StatementList  slChangePassword = slChangePasswordJb.getMethodStatementList();			
				slChangePassword.setSerial(serial+13160L);
				sList.add(slChangePassword);
				sList.add(new Statement(serial+13170L,0,","));
			}			
			
			Update update = new Update(this.domain,this.technicalStack,this.dbType);
			update.setDetailPrefix("");
			JavascriptBlock slUpdateJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)update).generateEasyUIJSButtonBlock():null;
			if (slUpdateJb != null) {
				StatementList  slUpdate =slUpdateJb.getMethodStatementList();
				slUpdate.setSerial(serial+13200L);
				sList.add(slUpdate);
				sList.add(new Statement(serial+13250L,0,","));
			}
			
			JavascriptBlock slSoftDeleteJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDelete(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slSoftDeleteJb != null) {
				StatementList  slSoftDelete =slSoftDeleteJb.getMethodStatementList();
				slSoftDelete.setSerial(serial+13300L);
				sList.add(slSoftDelete);
				sList.add(new Statement(serial+13320L,0,","));
			}
			
			JavascriptBlock slActivateJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Activate(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slActivateJb != null) {
				StatementList  slActivate =slActivateJb.getMethodStatementList();
				slActivate.setSerial(serial+13340L);
				sList.add(slActivate);
				sList.add(new Statement(serial+13342L,0,","));
			}	
			
			JavascriptBlock slCloneJb = this.domain.hasDomainId()?((EasyUIPositions)new Clone(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slCloneJb != null) {
				StatementList  slClone=slCloneJb.getMethodStatementList();
				slClone.setSerial(serial+13345L);
				sList.add(slClone);			
				sList.add(new Statement(serial+13350L,0,","));
			}
			
			JavascriptBlock slDeleteJb = this.domain.hasDomainId()?((EasyUIPositions)new Delete(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slDeleteJb != null) {
				StatementList  slDelete =slDeleteJb.getMethodStatementList();
				slDelete.setSerial(serial+13400L);
				sList.add(slDelete);
				sList.add(new Statement(serial+13450L,0,","));
			}
			
			JavascriptBlock slToggleJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Toggle(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slToggleJb != null) {
				StatementList  slToggle =slToggleJb.getMethodStatementList();
				slToggle.setSerial(serial+13500L);
				sList.add(slToggle);
				sList.add(new Statement(serial+13550L,0,","));
			}
			
			JavascriptBlock slToggleOneJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ToggleOne(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slToggleOneJb != null) {
				StatementList  slToggleOne =slToggleOneJb.getMethodStatementList();
				slToggleOne.setSerial(serial+13600L);
				sList.add(slToggleOne);
				sList.add(new Statement(serial+13650L,0,",'-',"));
			}
			
			JavascriptBlock slSoftDeleteAllJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDeleteAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slSoftDeleteAllJb != null) {
				StatementList  slSoftDeleteAll =slSoftDeleteAllJb.getMethodStatementList();
				slSoftDeleteAll.setSerial(serial+13700L);
				sList.add(slSoftDeleteAll);
				sList.add(new Statement(serial+13710L,0,","));
			}
			
			JavascriptBlock slActivateAllJb = this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ActivateAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slActivateAllJb != null) {
				StatementList  slActivateAll =slActivateAllJb.getMethodStatementList();
				slActivateAll.setSerial(serial+13720L);
				sList.add(slActivateAll);			
				sList.add(new Statement(serial+13722L,0,","));
			}
			
			JavascriptBlock slCloneAllJb = this.domain.hasDomainId()?((EasyUIPositions)new CloneAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slCloneAllJb != null) {
				StatementList  slCloneAll =slCloneAllJb.getMethodStatementList();
				slCloneAll.setSerial(serial+13725L);
				sList.add(slCloneAll);			
				sList.add(new Statement(serial+13750L,0,","));
			}
			
			JavascriptBlock slDeleteAllJb = this.domain.hasDomainId()?((EasyUIPositions)new DeleteAll(this.domain)).generateEasyUIJSButtonBlock():null;
			if (slDeleteAllJb != null) {
				StatementList  slDeleteAll =slDeleteAllJb.getMethodStatementList();
				slDeleteAll.setSerial(serial+13800L);
				sList.add(slDeleteAll);				
				sList.add(new Statement(serial+13850L,0,","));
			}
			
			JavascriptBlock slExportJb = ((EasyUIPositions)new Export(this.domain)).generateEasyUIJSButtonBlock();
			if (slExportJb != null) {
				StatementList  slExport =slExportJb.getMethodStatementList();
				slExport.setSerial(serial+13900L);
				sList.add(slExport);			
				sList.add(new Statement(serial+13950L,0,","));
			}
			
			JavascriptBlock slExportPDFJb = ((EasyUIPositions)new ExportPDF(this.domain)).generateEasyUIJSButtonBlock();
			if (slExportPDFJb != null) {
				StatementList  slExportPDF =slExportPDFJb.getMethodStatementList();
				slExportPDF.setSerial(serial+13960L);
				sList.add(slExportPDF);
				sList.add(new Statement(serial+13970L,0,","));
			}
			
			JavascriptBlock slExportWordJb = ((EasyUIPositions)new ExportWord(this.domain)).generateEasyUIJSButtonBlock();
			if (slExportWordJb != null) {
				StatementList  slExportWord =slExportWordJb.getMethodStatementList();
				slExportWord.setSerial(serial+13980L);
				sList.add(slExportWord);
				sList.add(new Statement(serial+13990L,0,","));
			}
			
			// remove last ','
			if (((Statement) sList.get(sList.size()-1)).getContent().contentEquals(",")) {
				sList.remove(sList.size()-1);
			}
			
			sList.add(new Statement(serial+14000L,0,"];"));
			sList.add(new Statement(serial+15000L,0,"$(document).ready(function(){"));
			sList.add(new Statement(serial+16000L,0,"$(\"#dg\").datagrid(\"load\");"));
			sList.add(new Statement(serial+17000L,0,"});"));
	
			sList.add(new Statement(serial+18000L,0,"function clearForm(formId){"));
			sList.add(new Statement(serial+19000L,0,"$('#'+formId).form('clear');"));
			sList.add(new Statement(serial+20000L,0,"}"));
			
			JavascriptMethod slAddJm = this.domain.hasActiveField()? ((EasyUIPositions)add).generateEasyUIJSActionMethod():null;
			if(	slAddJm != null) {
				StatementList slAddAction = slAddJm.generateMethodStatementListIncludingContainer(serial+21000L,0);
				sList.add(slAddAction);
			}		

			JavascriptMethod slUpdateJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)update).generateEasyUIJSActionMethod():null;
			if(	slUpdateJm != null) {
				StatementList slUpdateAction = slUpdateJm.generateMethodStatementListIncludingContainer(serial+22000L,0);
				sList.add(slUpdateAction);
			}
			
			JavascriptMethod slSoftDeleteJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDelete(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slSoftDeleteJm != null) {
				StatementList slSoftDeleteAction = slSoftDeleteJm.generateMethodStatementListIncludingContainer(serial+23000L,0);
				sList.add(slSoftDeleteAction);
			}
			
			JavascriptMethod slActivateJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Activate(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slActivateJm != null) {
				StatementList slActivateAction = slActivateJm.generateMethodStatementListIncludingContainer(serial+24000L,0);
				sList.add(slActivateAction);
			}
			
			JavascriptMethod slCloneJm =  this.domain.hasDomainId()?((EasyUIPositions)new Clone(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slCloneJm != null) {
				StatementList slCloneAction = slCloneJm.generateMethodStatementListIncludingContainer(serial+25000L,0);
				sList.add(slCloneAction);
			}
			
			JavascriptMethod slDeleteJm = this.domain.hasDomainId()? ((EasyUIPositions)new Delete(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slDeleteJm != null) {
				StatementList slDeleteAction = slDeleteJm.generateMethodStatementListIncludingContainer(serial+28000L,0);
				sList.add(slDeleteAction);
			}
			
			JavascriptMethod slToggleJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new Toggle(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slToggleJm != null) {
				StatementList slToggleAction = slToggleJm.generateMethodStatementListIncludingContainer(serial+28200L,0);
				sList.add(slToggleAction);
			}
			
			JavascriptMethod slToggleOneJm = this.domain.hasDomainId()&&this.domain.hasActiveField()? ((EasyUIPositions)new ToggleOne(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slToggleOneJm != null) {
				StatementList slToggleOneAction = slToggleOneJm.generateMethodStatementListIncludingContainer(serial+28400L,0);
				sList.add(slToggleOneAction);
			}
			
			JavascriptMethod slSoftDeleteAllJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new SoftDeleteAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slSoftDeleteAllJm != null) {
				StatementList slSoftDeleteAllAction = slSoftDeleteAllJm.generateMethodStatementListIncludingContainer(serial+28600L,0);
				sList.add(slSoftDeleteAllAction);
			}
			
			JavascriptMethod slActivateAllJm =  this.domain.hasDomainId()&&this.domain.hasActiveField()?((EasyUIPositions)new ActivateAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slActivateAllJm != null) {
				StatementList slActivateAllAction = slActivateAllJm.generateMethodStatementListIncludingContainer(serial+28800L,0);
				sList.add(slActivateAllAction);
			}
			
			JavascriptMethod slCloneAllJm = this.domain.hasDomainId()? ((EasyUIPositions)new CloneAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slCloneAllJm != null) {
				StatementList slCloneAllAction = slCloneAllJm.generateMethodStatementListIncludingContainer(serial+29000L,0);
				sList.add(slCloneAllAction);
			}
			
			JavascriptMethod slDeleteAllJm =  this.domain.hasDomainId()? ((EasyUIPositions)new DeleteAll(this.domain)).generateEasyUIJSActionMethod():null;
			if(	slDeleteAllJm != null) {
				StatementList slDeleteAllAction = slDeleteAllJm.generateMethodStatementListIncludingContainer(serial+29200L,0);
				sList.add(slDeleteAllAction);
			}
			
			JavascriptMethod slSearchByFieldsByPageJm =  ((EasyUIPositions)new SearchByFieldsByPage(this.domain)).generateEasyUIJSActionMethod();
			if(	slSearchByFieldsByPageJm != null) {
				StatementList slSearchByFieldsByPageAction = slSearchByFieldsByPageJm.generateMethodStatementListIncludingContainer(serial+29400L,0);
				sList.add(slSearchByFieldsByPageAction);
			}
			
			JavascriptMethod slFilterExcelJm =  ((EasyUIPositions)new FilterExcelFilterFields(this.domain)).generateEasyUIJSActionMethod();
			if(	slFilterExcelJm != null) {
				StatementList slFilterExcelAction = slFilterExcelJm.generateMethodStatementListIncludingContainer(serial+29600L,0);
				sList.add(slFilterExcelAction);
			}
			
			JavascriptMethod slFilterPDFJm =  ((EasyUIPositions)new FilterPDFFilterFields(this.domain)).generateEasyUIJSActionMethod();
			if(	slFilterPDFJm != null) {
				StatementList slFilterPDFAction = slFilterPDFJm.generateMethodStatementListIncludingContainer(serial+29800L,0);
				sList.add(slFilterPDFAction);
			}
			
			JavascriptMethod slFilterWordJm =  ((EasyUIPositions)new FilterWordFilterFields(this.domain)).generateEasyUIJSActionMethod();
			if(	slFilterWordJm != null) {
				StatementList slFilterWordAction = slFilterWordJm.generateMethodStatementListIncludingContainer(serial+29900L,0);
				sList.add(slFilterWordAction);
			}
			
			JavascriptMethod slChangePasswordUserJm =  ((EasyUIPositions)new ChangePasswordUser(this.domain)).generateEasyUIJSActionMethod();
			if(	slChangePasswordUserJm != null) {
				StatementList slChangePasswordUserAction = slChangePasswordUserJm.generateMethodStatementListIncludingContainer(serial+29950L,0);
				sList.add(slChangePasswordUserAction);
			}
			
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseBooleanMethod().generateMethodStatementList(serial+30000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateParseBooleanIntMethod().generateMethodStatementList(serial+30500L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateIsBlankMethod().generateMethodStatementList(serial+31000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateCheckRadioBoxValueMethod().generateMethodStatementList(serial+32000L));
			sList.add(NamedS2SMJavascriptMethodGenerator.generateToggleBtnShowMethod().generateMethodStatementList(serial+32500L));
			
			serial = serial+33000L;
			List<Field> fields5 = new ArrayList<Field>();
			fields5.addAll(this.domain.getFieldsWithoutIdAndActive());
			fields5.sort(new FieldSerialComparator());
			List<Domain> translateDomains = new ArrayList<Domain>();
			for (Field f: fields5){
				if (f instanceof Dropdown){
					Dropdown dp = (Dropdown)f;
					Domain target = dp.getTarget();
					if (!target.isLegacy()&&	!DomainUtil.inDomainList(target, translateDomains)){
						sList.add(dp.generateTranslateMethod().generateMethodStatementList(serial));
						serial+=1000L;
						translateDomains.add(target);
					}
				}
			}
			
			for (Field f: fields5){
				if (f.getFieldType().equalsIgnoreCase("image")) {
					AddUploadDomainField audf = new AddUploadDomainField(this.domain,f);
					sList.add(audf.generateEasyUIJSActionMethod().generateMethodStatementList(serial));
					serial+=1000L;
				}
			}
			StatementList mylist = WriteableUtil.merge(sList);
			return mylist;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}


	public Set<DomainFieldVerb> getDomainFieldVerbs() {
		return domainFieldVerbs;
	}

	public void setDomainFieldVerbs(Set<DomainFieldVerb> domainFieldVerbs) {
		this.domainFieldVerbs = domainFieldVerbs;
	}

	public Set<Verb> getVerbs() {
		return verbs;
	}

	public void setVerbs(Set<Verb> verbs) {
		this.verbs = verbs;
	}
}
