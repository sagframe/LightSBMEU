package org.light.shiroauth;

import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Util;

public class UserRegisteAndLoginGenerator extends Util{
	protected Domain userDomain;
	public UserRegisteAndLoginGenerator(){
		super();
		super.fileName = "UserRegisteAndLogin.java";
	}
	
	public UserRegisteAndLoginGenerator(String packageToken){
		super();
		this.setPackageToken(packageToken);
		super.fileName = "UserRegisteAndLogin.java";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		StatementList sList = new StatementList();
		sList.add(new Statement(500L,"package "+this.getPackageToken()+".shiro;"));
		sList.add(new Statement(1000L,0,"import org.apache.shiro.SecurityUtils;"));
		sList.add(new Statement(2000L,0,"import org.apache.shiro.authc.IncorrectCredentialsException;"));
		sList.add(new Statement(3000L,0,"import org.apache.shiro.authc.UnknownAccountException;"));
		sList.add(new Statement(4000L,0,"import org.apache.shiro.authc.UsernamePasswordToken;"));
		sList.add(new Statement(5000L,0,"import org.apache.shiro.crypto.SecureRandomNumberGenerator;"));
		sList.add(new Statement(6000L,0,"import org.apache.shiro.crypto.hash.Sha1Hash;"));
		sList.add(new Statement(7000L,0,"import org.apache.shiro.subject.Subject;"));
		sList.add(new Statement(8000L,0,""));
		sList.add(new Statement(9000L,0,"import "+this.userDomain.getPackageToken()+"."+this.userDomain.getDomainSuffix()+"."+this.userDomain.getCapFirstDomainNameWithSuffix()+";"));
		sList.add(new Statement(10000L,0,""));
		sList.add(new Statement(11000L,0,"/**"));
		sList.add(new Statement(12000L,0,"*用户注册与登录时用到的函数"));
		sList.add(new Statement(13000L,0,"*/"));
		sList.add(new Statement(14000L,0,"public class UserRegisteAndLogin"));
		sList.add(new Statement(15000L,0,"{"));
		sList.add(new Statement(16000L,1,"/**"));
		sList.add(new Statement(17000L,1,"* 用户注册时加密用户的密码"));
		sList.add(new Statement(18000L,1,"* 输入密码明文 返回密文与盐值"));
		sList.add(new Statement(19000L,1,"* @param password"));
		sList.add(new Statement(20000L,1,"* @return 第一个是密文  第二个是盐值"));
		sList.add(new Statement(21000L,1,"*/"));
		sList.add(new Statement(22000L,1,"public static String[] encryptPassword(String password)"));
		sList.add(new Statement(23000L,1,"{"));
		sList.add(new Statement(24000L,2,"String salt = new SecureRandomNumberGenerator().nextBytes().toHex(); //生成盐值"));
		sList.add(new Statement(25000L,2,"String ciphertext = new Sha1Hash(password,salt,3).toString(); //生成的密文"));
		sList.add(new Statement(26000L,0,""));
		sList.add(new Statement(27000L,2,"String[] strings = new String[]{salt, ciphertext};"));
		sList.add(new Statement(28000L,0,""));
		sList.add(new Statement(29000L,2,"return strings;"));
		sList.add(new Statement(30000L,1,"}"));
		sList.add(new Statement(31000L,0,""));
		sList.add(new Statement(32000L,1,"/**"));
		sList.add(new Statement(33000L,1,"* 获得本次输入的密码的密文"));
		sList.add(new Statement(34000L,1,"* @param password"));
		sList.add(new Statement(35000L,1,"* @param salt"));
		sList.add(new Statement(36000L,1,"* @return"));
		sList.add(new Statement(37000L,1,"*/"));
		sList.add(new Statement(38000L,1,"public static String getInputPasswordCiph(String password, String salt)"));
		sList.add(new Statement(39000L,1,"{"));
		sList.add(new Statement(40000L,2,"if(salt == null)"));
		sList.add(new Statement(41000L,2,"{"));
		sList.add(new Statement(42000L,3,"password = \"\";"));
		sList.add(new Statement(43000L,2,"}"));
		sList.add(new Statement(44000L,0,""));
		sList.add(new Statement(45000L,2,"String ciphertext = new Sha1Hash(password,salt,3).toString(); //生成的密文"));
		sList.add(new Statement(46000L,0,""));
		sList.add(new Statement(47000L,2,"return ciphertext;"));
		sList.add(new Statement(48000L,1,"}"));
		sList.add(new Statement(49000L,0,""));
		sList.add(new Statement(50000L,1,"/**"));
		sList.add(new Statement(51000L,1,"* 用户登录函数，在controller里调用"));
		sList.add(new Statement(52000L,1,"* @param user"));
		sList.add(new Statement(53000L,1,"* @param model"));
		sList.add(new Statement(54000L,1,"* @return"));
		sList.add(new Statement(55000L,1,"*/"));
		sList.add(new Statement(56000L,1,"public static String userLogin("+this.userDomain.getCapFirstDomainNameWithSuffix()+" user)"));
		sList.add(new Statement(57000L,1,"{"));
		sList.add(new Statement(58000L,2,"Subject subject = SecurityUtils.getSubject(); //获得Subject对象"));
		sList.add(new Statement(59000L,2,"UsernamePasswordToken token = new UsernamePasswordToken(user."+this.userDomain.findFieldByFixedName("userName").getGetterCallName()+"(), user."+this.userDomain.findFieldByFixedName("password").getGetterCallName()+"()); //将用户输入的用户名写密码封装到一个UsernamePasswordToken对象中"));
		sList.add(new Statement(60000L,0,""));
		sList.add(new Statement(61000L,2,"//用Subject对象执行登录方法，没有抛出任何异常说明登录成功"));
		sList.add(new Statement(62000L,2,"try"));
		sList.add(new Statement(63000L,2,"{"));
		sList.add(new Statement(64000L,3,"subject.login(token); //login()方法会调用 Realm类中执行认证逻辑的方法，并将这个参数传递给doGetAuthenticationInfo()方法"));
		sList.add(new Statement(65000L,3,"return \"index\";"));
		sList.add(new Statement(66000L,2,"}"));
		sList.add(new Statement(67000L,2,"catch (UnknownAccountException e) //抛出这个异常说明用户不存在"));
		sList.add(new Statement(68000L,2,"{"));
		sList.add(new Statement(69000L,3,"return \"login\";"));
		sList.add(new Statement(70000L,2,"}"));
		sList.add(new Statement(71000L,2,"catch (IncorrectCredentialsException e) //抛出这个异常说明密码错误"));
		sList.add(new Statement(72000L,2,"{"));
		sList.add(new Statement(73000L,3,"return \"login\";"));
		sList.add(new Statement(74000L,2,"}"));
		sList.add(new Statement(75000L,1,"}"));
		sList.add(new Statement(76000L,1,""));
		sList.add(new Statement(77000L,1,"public static void main(String [] args) {"));
		sList.add(new Statement(78000L,2,"try {"));
		sList.add(new Statement(79000L,2,"String encPassword = new Sha1Hash(\"admin\",\"\",1).toString();"));
		sList.add(new Statement(80000L,2,"System.out.println(encPassword);"));
		sList.add(new Statement(81000L,2,"String [] ret = UserRegisteAndLogin.encryptPassword(encPassword);"));
		sList.add(new Statement(82000L,2,"System.out.println(ret[0]+\":\"+ret[1]);"));
		sList.add(new Statement(83000L,2,"} catch(Exception e) {"));
		sList.add(new Statement(84000L,3,"e.printStackTrace();"));
		sList.add(new Statement(85000L,2,"}"));
		sList.add(new Statement(86000L,1,"}"));
		sList.add(new Statement(87000L,0,"}"));

		return sList.getContent();
	}

	public Domain getUserDomain() {
		return userDomain;
	}

	public void setUserDomain(Domain userDomain) {
		this.userDomain = userDomain;
	}

}
