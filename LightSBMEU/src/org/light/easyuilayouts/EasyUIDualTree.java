package org.light.easyuilayouts;

import org.light.domain.Domain;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.TreePanel;

public class EasyUIDualTree extends EasyUILayout{
	protected Domain leftTreeDomain;
	protected Domain rightTreeDomain;
	protected TreePanel leftTree;
	protected TreePanel rightTree;

	@Override
	public StatementList generateLayoutStatements() {
		// TODO Auto-generated method stub
		return null;
	}

	public Domain getLeftTreeDomain() {
		return leftTreeDomain;
	}

	public void setLeftTreeDomain(Domain leftTreeDomain) {
		this.leftTreeDomain = leftTreeDomain;
	}

	public Domain getRightTreeDomain() {
		return rightTreeDomain;
	}

	public void setRightTreeDomain(Domain rightTreeDomain) {
		this.rightTreeDomain = rightTreeDomain;
	}

	@Override
	public StatementList generateLayoutScriptStatements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean parse() {
		// TODO Auto-generated method stub
		return false;
	}

	public TreePanel getLeftTree() {
		return leftTree;
	}

	public void setLeftTree(TreePanel leftTree) {
		this.leftTree = leftTree;
	}

	public TreePanel getRightTree() {
		return rightTree;
	}

	public void setRightTree(TreePanel rightTree) {
		this.rightTree = rightTree;
	}

}
