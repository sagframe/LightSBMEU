package org.light.easyuilayouts;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.AddDialog;
import org.light.easyuilayouts.widgets.MainDatagrid;
import org.light.easyuilayouts.widgets.OracleMainDatagrid;
import org.light.easyuilayouts.widgets.SearchPanel;
import org.light.easyuilayouts.widgets.TreePanel;
import org.light.easyuilayouts.widgets.UpdateDialog;
import org.light.easyuilayouts.widgets.ViewDialog;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;

public class OracleEasyUITreeGrid extends EasyUITreeGrid{
	protected OracleMainDatagrid mainDatagrid;
	
	@Override
	public StatementList generateLayoutStatements() {
		List<Writeable> sList = new ArrayList<>();
		sList.add(new Statement(1000L,0,"<div style=\"width:15%;float:left\">"));
		StatementList sl = tree.generateWidgetStatements();
		sl.setSerial(2000L);
		sl.setIndent(1);
		sList.add(sl);
		sList.add(new Statement(3000L,0,"</div>"));
		sList.add(new Statement(4000L,0,"<div style=\"width:85%;float:right;\">"));
		StatementList sl1 = searchPanel.generateWidgetStatements();
		sl1.setSerial(4000L);
		StatementList sl2 = mainDatagrid.generateWidgetStatements();
		sl2.setSerial(5000L);		
		StatementList sl3 = addDialog.generateWidgetStatements();
		sl3.setSerial(6000L);
		StatementList sl4 = updateDialog.generateWidgetStatements();
		sl4.setSerial(7000L);
		StatementList sl5 = viewDialog.generateWidgetStatements();
		sl5.setSerial(8000L);
		sList.add(sl1);
		sList.add(sl2);
		sList.add(sl3);
		sList.add(sl4);
		sList.add(sl5);	
		sList.add(new Statement(9000L,0,"</div>"));
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
		StatementList sl = tree.generateWidgetScriptStatements();
		sl.setSerial(1000L);
		sl.setIndent(1);
		sList.add(sl);
		StatementList sl1 = mainDatagrid.generateWidgetScriptStatements();
		sl1.setSerial(2000L);
		sl1.setIndent(1);
		sList.add(sl1);
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public boolean parse() {
		if (this.parentTreeDomain!=null&&this.childDomain!=null) {
			this.tree = new TreePanel();
			this.searchPanel = new SearchPanel();
			this.mainDatagrid = new OracleMainDatagrid();
			this.addDialog = new AddDialog();
			this.updateDialog = new UpdateDialog();
			this.viewDialog = new ViewDialog();
			
			this.tree.setParentTreeDomain(this.parentTreeDomain);
			this.tree.setInnerTreeParentId(this.innerTreeParentId);
			this.tree.setTreeParentId(this.treeParentId);
			this.tree.setChildDomain(this.childDomain);
			this.searchPanel.setDomain(this.childDomain);
			this.mainDatagrid.setDomain(this.childDomain);
			this.addDialog.setDomain(this.childDomain);
			this.updateDialog.setDomain(this.childDomain);
			this.viewDialog.setDomain(this.childDomain);
			return true;
		}
		return false;
	}

}
