package org.light.easyuilayouts;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.easyuilayouts.widgets.AddDialog;
import org.light.easyuilayouts.widgets.MainDatagrid;
import org.light.easyuilayouts.widgets.SearchPanel;
import org.light.easyuilayouts.widgets.UpdateDialog;
import org.light.easyuilayouts.widgets.ViewDialog;
import org.light.exception.ValidateException;
import org.light.utils.WriteableUtil;

public class EasyUIHomePageLayout extends EasyUILayout{
	protected Domain domain;
	
	public EasyUIHomePageLayout(Domain domain) {
		super();
		this.domain = domain;
	}

	@Override
	public StatementList generateLayoutStatements() {
		List<Writeable> sList = new ArrayList<>();
		if (this.getLanguage().equalsIgnoreCase("english")) {
			sList.add(new Statement(2000L,0,"<h2>Welcome to the verb operator code generator world!</h2>"));
		} else {
			sList.add(new Statement(2000L,0,"<h2>欢迎来到第三代动词算子式代码生成器：光的世界！</h2>"));
		}
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws ValidateException{
		List<Writeable> sList = new ArrayList<>();
	
		StatementList rsl = WriteableUtil.merge(sList);
		rsl.setSerial(this.serial);
		return rsl;
	}

	@Override
	public boolean parse() {
		if (this.domain!=null) {
			return true;
		}
		return false;
	}

}
