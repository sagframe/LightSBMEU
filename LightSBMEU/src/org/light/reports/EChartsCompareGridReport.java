package org.light.reports;

import java.util.List;

import org.light.core.ReportComb;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.ValidateInfo;
import org.light.easyuilayouts.EasyUIFrameSet;
import org.light.easyuilayouts.widgets.Footer;
import org.light.easyuilayouts.widgets.Header;
import org.light.easyuilayouts.widgets.Nav;
import org.light.utils.WriteableUtil;

public class EChartsCompareGridReport extends ReportComb{
	private static final long serialVersionUID = 1L;
	protected EasyUIFrameSet frame = new EasyUIFrameSet();
	protected EchartsCompareGridReportLayout eLayout = new EchartsCompareGridReportLayout();
	protected Domain  planDomain;
	protected Domain actionDomain;
	protected List<Field> planxAxisFields;
	protected Field planyName;
	protected List<Field> actionxAxisFields;
	protected Field actionyName;
	
	public EChartsCompareGridReport(Domain planDomain,List<Field> planxAxisFields,Field planyName,Domain actionDomain,List<Field> actionxAxisFields,Field actionyName) throws Exception {
		super();
		this.standardName = "EchartsCompareGridReport"+planDomain.getCapFirstDomainName()+actionDomain.getCapFirstDomainName();
		this.label = planDomain.getText()+ actionDomain.getText() +"Echarts比较网格图表";
		if ("english".equalsIgnoreCase(planDomain.getLanguage())) {
			this.label ="Echarts compare grod report for"+planDomain.getText() +" and " +actionDomain.getText();
		}
		this.planDomain = planDomain;
		this.planxAxisFields = planxAxisFields;
		this.planyName = planyName;
		this.actionDomain = actionDomain;
		this.actionxAxisFields = actionxAxisFields;
		this.actionyName = actionyName;
		
		this.domains.add(this.planDomain);
		this.domains.add(this.actionDomain);
		
		eLayout.setPlanDomain(this.planDomain);
		eLayout.setPlanxAxisFields(this.planxAxisFields);
		eLayout.setPlanyName(this.planyName);
		
		eLayout.setActionDomain(this.actionDomain);
		eLayout.setActionxAxisFields(this.actionxAxisFields);
		eLayout.setActionyName(this.actionyName);
		
		eLayout.parse();
		
		StatementList sl = new StatementList();
		sl.add(new Statement(1000L,0,"<script type=\"text/javascript\" src=\"../echarts/echarts.min.js\"></script>"));
		frame.setAdditionScriptFiles(sl);
		frame.setMainContent(eLayout);
		frame.setStandardName(this.label);
		frame.setLanguage(this.planDomain.getLanguage());
	}

	public Domain getPlanDomain() {
		return planDomain;
	}

	public void setPlanDomain(Domain planDomain) {
		this.planDomain = planDomain;
	}

	public Domain getActionDomain() {
		return actionDomain;
	}

	public void setActionDomain(Domain actionDomain) {
		this.actionDomain = actionDomain;
	}

	@Override
	public void generateCombFiles(String targetFolderPath) throws Exception {
		String relativeFolder = "WebContent/pages/";
		String relativeFolder0 = "src/main/resources/static/pages/";
		if ("smeu".equalsIgnoreCase(this.technicalStack)||"msmeu".equalsIgnoreCase(this.technicalStack)) {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder+this.standardName.toLowerCase()+".html", frame.generateFrameSetStatementList().getContent());
		}else {
			WriteableUtil.writeToFile(targetFolderPath + "/"+relativeFolder0+this.standardName.toLowerCase()+".html", frame.generateFrameSetStatementList().getContent());
		}
	}

	@Override
	public void generateCombFromDomians() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ValidateInfo validateDomains() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean validateVerbs() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateLayouts() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void setTitles(String title, String subTitle,String footer) {
		Header header = (Header)this.frame.getHeader();
		header.setTitle(title);
		header.setSubTitle(subTitle);
		Footer pagefooter = (Footer) this.frame.getPageFooter();
		pagefooter.setFooterStr(footer);
	}
	
	@Override
	public void setNav(Nav nav) {
		super.setNav(nav);
		this.frame.setNav(nav);
	}
	
	@Override
	public String getDomainNamesStr() {
		return this.planDomain.getStandardName()+","+this.actionDomain.getStandardName();
	}
}
