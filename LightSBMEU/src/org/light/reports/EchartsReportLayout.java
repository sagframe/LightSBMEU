package org.light.reports;

import java.util.List;

import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.StatementList;
import org.light.easyuilayouts.EasyUILayout;
import org.light.reports.verbs.ListActiveCompareOption;
import org.light.reports.verbs.ListActiveCompareSumOption;
import org.light.reports.verbs.ListActiveOption;
import org.light.reports.verbs.ListActiveSumOption;

public class EchartsReportLayout extends EasyUILayout{
	protected Domain reportDomain;
	protected List<Field> xAxisFields;
	protected Field yName;
	protected EchartsDiagram diagram;

	@Override
	public StatementList generateLayoutStatements() throws Exception{		
		StatementList sl = diagram.generateWidgetStatements();
		sl.setSerial(this.serial);
		return sl;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws Exception{
		StatementList sl =  diagram.generateWidgetScriptStatements();
		sl.setSerial(this.serial);
		return sl;
	}

	@Override
	public boolean parse() throws Exception{
		if (this.diagram == null) {
			ListActiveOption lao = new ListActiveOption(this.reportDomain,this.xAxisFields,this.yName);
			ListActiveSumOption laso = new ListActiveSumOption(this.reportDomain,this.xAxisFields,this.yName);
			
			this.diagram = new EchartsDiagram();
			this.diagram.setListOption(lao.generateEasyUIJSActionMethod());
			this.diagram.setListSumOption(laso.generateEasyUIJSActionMethod());
			
			this.diagram.setDomain(this.reportDomain);
			this.diagram.setxAxisFields(this.xAxisFields);
			this.diagram.setyName(this.yName);
		}
		return true;
	}

	public Domain getReportDomain() {
		return reportDomain;
	}

	public void setReportDomain(Domain reportDomain) {
		this.reportDomain = reportDomain;
	}

	public List<Field> getxAxisFields() {
		return xAxisFields;
	}

	public void setxAxisFields(List<Field> xAxisFields) {
		this.xAxisFields = xAxisFields;
	}

	public Field getyName() {
		return yName;
	}

	public void setyName(Field yName) {
		this.yName = yName;
	}

	public EchartsDiagram getDiagram() {
		return diagram;
	}

	public void setDiagram(EchartsDiagram diagram) {
		this.diagram = diagram;
	}

	
}
