package org.light.reports;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.StatementList;
import org.light.easyuilayouts.EasyUILayout;
import org.light.easyuilayouts.widgets.ViewDialog;
import org.light.reports.verbs.ListActiveCompareOption;
import org.light.reports.verbs.ListActiveCompareSumOption;
import org.light.utils.WriteableUtil;

public class EchartsCompareGridReportLayout extends EasyUILayout{
	protected Domain planDomain;
	protected Domain actionDomain;
	protected List<Field> planxAxisFields;
	protected Field planyName;
	protected List<Field> actionxAxisFields;
	protected Field actionyName;
	protected EchartsCompareDiagram diagram;
	protected DiagramDataControlGrid planGrid;
	protected DiagramDataControlGrid actionGrid;
	protected ViewDialog viewPlanDialog;
	protected ViewDialog viewActionDialog;

	@Override
	public StatementList generateLayoutStatements() throws Exception{	
		this.parse();
		List<Writeable> sList = new ArrayList<>();
		StatementList sl = this.diagram.generateWidgetStatements();
		sl.setSerial(1000L);
		StatementList sl2 = this.planGrid.generateWidgetStatements();
		sl2.setSerial(2000L);
		
		StatementList sl22 = this.actionGrid.generateWidgetStatements();
		sl22.setSerial(2500L);
		
		StatementList sl3 = this.viewPlanDialog.generateWidgetStatements();
		sl3.setSerial(3000L);
		
		StatementList sl4 = this.viewActionDialog.generateWidgetStatements();
		sl4.setSerial(4000L);
		
		sList.add(sl);
		sList.add(sl2);
		sList.add(sl22);
		sList.add(sl3);
		sList.add(sl4);
		StatementList result =  WriteableUtil.merge(sList);
		result.setSerial(this.serial);
		return result;
	}

	@Override
	public StatementList generateLayoutScriptStatements() throws Exception{
		this.parse();
		List<Writeable> sList = new ArrayList<>();
		StatementList sl =  this.diagram.generateWidgetScriptStatements();
		sl.setSerial(1000L);
		this.planGrid.setParentTranlateDomains(this.diagram.getTranlateDomains());
		StatementList sl2 = this.planGrid.generateWidgetScriptStatements();
		sl2.setSerial(2000L);
		this.actionGrid.setParentTranlateDomains(this.planGrid.getTranlateDomains());
		StatementList sl3 = this.actionGrid.generateWidgetScriptStatements();
		sl2.setSerial(3000L);
		
//		StatementList sl3 = viewDialog.generateWidgetScriptStatements();
//		sl3.setSerial(3000L);
		
		sList.add(sl);
		sList.add(sl2);
		sList.add(sl3);
		StatementList result =  WriteableUtil.merge(sList);
		result.setSerial(this.serial);
		return result;
	}

	@Override
	public boolean parse() throws Exception{
		if (this.diagram == null) {
			ListActiveCompareOption laco = new ListActiveCompareOption(this.planDomain,this.planxAxisFields,this.planyName,this.actionDomain,this.actionxAxisFields,this.actionyName);
			ListActiveCompareSumOption lacso = new ListActiveCompareSumOption(this.planDomain,this.planxAxisFields,this.planyName,this.actionDomain,this.actionxAxisFields,this.actionyName);
			this.diagram = new EchartsCompareDiagram();
			this.diagram.setListOption(laco.generateEasyUIJSActionMethod());
			this.diagram.setListSumOption(lacso.generateEasyUIJSActionMethod());
			this.diagram.setPlanDomain(this.planDomain);
			this.diagram.setPlanxAxisFields(this.planxAxisFields);
			this.diagram.setPlanyName(this.planyName);
			this.diagram.setActionDomain(this.actionDomain);
			this.diagram.setActionxAxisFields(this.actionxAxisFields);
			this.diagram.setActionyName(this.actionyName);
			
			this.planGrid = new DiagramDataControlGrid("");
			this.planGrid.setDomain(this.planDomain);
			
			this.actionGrid = new DiagramDataControlGrid("action");
			this.actionGrid.setDomain(this.actionDomain);
		
			this.viewPlanDialog = new ViewDialog();
			this.viewPlanDialog.setDomain(this.planDomain);
			
			this.viewActionDialog = new ViewDialog();
			this.viewActionDialog.setDomain(this.actionDomain);
			this.viewActionDialog.setDetailPrefix("action");
		}
		return true;		
	}

	public Domain getPlanDomain() {
		return planDomain;
	}

	public void setPlanDomain(Domain planDomain) {
		this.planDomain = planDomain;
	}

	public Domain getActionDomain() {
		return actionDomain;
	}

	public void setActionDomain(Domain actionDomain) {
		this.actionDomain = actionDomain;
	}

	public List<Field> getPlanxAxisFields() {
		return planxAxisFields;
	}

	public void setPlanxAxisFields(List<Field> planxAxisFields) {
		this.planxAxisFields = planxAxisFields;
	}

	public Field getPlanyName() {
		return planyName;
	}

	public void setPlanyName(Field planyName) {
		this.planyName = planyName;
	}

	public Field getActionyName() {
		return actionyName;
	}

	public void setActionyName(Field actionyName) {
		this.actionyName = actionyName;
	}

	public EchartsCompareDiagram getDiagram() {
		return diagram;
	}

	public void setDiagram(EchartsCompareDiagram diagram) {
		this.diagram = diagram;
	}

	public DiagramDataControlGrid getPlanGrid() {
		return planGrid;
	}

	public void setPlanGrid(DiagramDataControlGrid planGrid) {
		this.planGrid = planGrid;
	}

	public DiagramDataControlGrid getActionGrid() {
		return actionGrid;
	}

	public void setActionGrid(DiagramDataControlGrid actionGrid) {
		this.actionGrid = actionGrid;
	}

	public ViewDialog getViewPlanDialog() {
		return viewPlanDialog;
	}

	public void setViewPlanDialog(ViewDialog viewPlanDialog) {
		this.viewPlanDialog = viewPlanDialog;
	}

	public ViewDialog getViewActionDialog() {
		return viewActionDialog;
	}

	public void setViewActionDialog(ViewDialog viewActionDialog) {
		this.viewActionDialog = viewActionDialog;
	}

	public List<Field> getActionxAxisFields() {
		return actionxAxisFields;
	}

	public void setActionxAxisFields(List<Field> actionxAxisFields) {
		this.actionxAxisFields = actionxAxisFields;
	}
	
}
