package org.light.generator;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Independent;
import org.light.domain.Statement;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ApplicationJavaGenerator extends Independent{
	private static final long serialVersionUID = -9171617426302332622L;

	public ApplicationJavaGenerator(){
		super();
		this.fileName = "Application.java";
		this.standardName = "Application";
	}
	
	public ApplicationJavaGenerator(String packageToken){
		super();
		this.packageToken = packageToken;
		this.fileName = "Application.java";
		this.standardName = "Application";
	}
	
	@Override
	public void setPackageToken(String packageToken){
		this.packageToken = packageToken+"";
	}
	
	@Override
	public String generateImplString() {
		List<Writeable> sList = new ArrayList<Writeable>();
		if (!StringUtil.isBlank(this.packageToken)) {
			sList.add(new Statement(1000L,0,"package "+this.packageToken+";"));
			sList.add(new Statement(2000L,0,""));
		}
		sList.add(new Statement(3000L,0,"import org.springframework.boot.SpringApplication;"));
		sList.add(new Statement(4000L,0,"import org.springframework.boot.autoconfigure.SpringBootApplication;"));
		sList.add(new Statement(5000L,0,"import org.springframework.boot.builder.SpringApplicationBuilder;"));
		sList.add(new Statement(6000L,0,"import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;"));
		sList.add(new Statement(7000L,0,""));
		sList.add(new Statement(8000L,0,"@SpringBootApplication"));
		sList.add(new Statement(9000L,0,"public class Application extends SpringBootServletInitializer{"));
		sList.add(new Statement(10000L,0,""));
		sList.add(new Statement(11000L,1,"public static void main(String[] args){"));
		sList.add(new Statement(12000L,2,"SpringApplication.run(Application.class, args);"));
		sList.add(new Statement(13000L,1,"}"));
		sList.add(new Statement(14000L,1,""));
		sList.add(new Statement(15000L,1,"@Override"));
		sList.add(new Statement(16000L,1,"protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {"));
		sList.add(new Statement(17000L,2,"return builder.sources(Application.class);"));
		sList.add(new Statement(18000L,1,"}"));
		sList.add(new Statement(19000L,0,"}"));
		return WriteableUtil.merge(sList).getContent();
	}
}
