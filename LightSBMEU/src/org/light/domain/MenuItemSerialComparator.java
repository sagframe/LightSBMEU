package org.light.domain;

import java.io.Serializable;
import java.util.Comparator;

public class MenuItemSerialComparator implements Comparator<MenuItem>,Serializable{
	private static final long serialVersionUID = -3073819745256627215L;

	@Override
	public int compare(MenuItem o1, MenuItem o2) {
		return ((Long)o1.getSerial()).compareTo(o2.getSerial());
	}

}
