package org.light.domain;

import java.io.Serializable;
import java.util.Comparator;

import org.light.core.PrismInterface;

public class PrismInterfaceSerialComparator implements Comparator<PrismInterface>,Serializable{
	private static final long serialVersionUID = 6633632682260828817L;

	@Override
	public int compare(PrismInterface o1, PrismInterface o2) {
		return ((Long)o1.getSerial()).compareTo(o2.getSerial());
	}
}
