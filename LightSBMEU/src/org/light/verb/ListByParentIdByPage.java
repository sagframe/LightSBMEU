package org.light.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.easyui.EasyUIPositions;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ListByParentIdByPage extends Verb implements EasyUIPositions {

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return null;
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,1,"<select id=\"list"+this.domain.getCapFirstPlural()+"ByParentIdByLimit\" resultMap=\""+this.domain.getLowerFirstDomainName()+"\">"));
			sList.add(new Statement(2000L,2,"select id,detail_name,active,parent_id,photo,description from cpe_product_details where parent_id = #{parentId} order by id asc"));
			sList.add(new Statement(3000L,2,"limit #{limit} offset #{start}"));
			sList.add(new Statement(4000L,1,"</select>"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		if (this.denied) return null;
		else {
			return generateDaoImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		if (this.denied) return null;
		else {
			Method m = this.generateDaoImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		if (this.denied) return null;
		else {
			return generateDaoMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		if (this.denied) return null;
		else {
			return generateServiceMethodDefinition().generateMethodDefinition();
		}
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,2,"Integer start = (pagenum-1)*pagesize;"));
			sList.add(new Statement(2000L,2,"Integer limit = pagesize;"));
			sList.add(new Statement(3000L,2,"return dao.listProductDetailsByParentIdByLimit(parentId,start,limit);"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}

	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		if (this.denied) return null;
		else {
			return generateServiceImplMethod().generateMethodString();
		}
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		if (this.denied) return null;
		else {
			Method m = this.generateServiceImplMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		if (this.denied) return null;
		else {
			Method method = new Method();
			method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));
			List<Writeable> sList = new ArrayList<Writeable>();
			sList.add(new Statement(1000L,2,"Map<String,Object> result = new TreeMap<String,Object>();"));
			sList.add(new Statement(2000L,2,"if (parentId==null) {"));
			sList.add(new Statement(3000L,3,"result.put(\"success\",true);"));
			sList.add(new Statement(4000L,3,"result.put(\"rows\",new ArrayList<"+this.domain.getCapFirstDomainName()+">());"));
			sList.add(new Statement(5000L,3,"return result;"));
			sList.add(new Statement(6000L,2,"}"));
			sList.add(new Statement(7000L,2,"Integer recordCount = service.count"+this.domain.getCapFirstPlural()+"ByParentId(parentId);"));
			sList.add(new Statement(8000L,2,"Integer pageCount = (int)Math.ceil((double)recordCount/pagesize);"));
			sList.add(new Statement(9000L,2,"if (pageCount <= 1) pageCount = 1;"));
			sList.add(new Statement(10000L,2,"if (pagenum==null || pagenum <= 1) pagenum = 1;"));
			sList.add(new Statement(11000L,2,"if (pagenum >= pageCount) pagenum = pageCount;"));
			sList.add(new Statement(12000L,2,"List<"+this.domain.getCapFirstDomainName()+"> list = service.list"+this.domain.getCapFirstPlural()+"ByParentIdByPage(parentId,pagesize,pagenum);"));
			sList.add(new Statement(13000L,2,"result.put(\"success\",true);"));
			sList.add(new Statement(14000L,2,"result.put(\"rows\",list);"));
			sList.add(new Statement(15000L,2,"result.put(\"total\",recordCount);"));
			sList.add(new Statement(16000L,2,"result.put(\"page\",pagenum);"));
			sList.add(new Statement(17000L,2,"return result;"));
			method.setMethodStatementList(WriteableUtil.merge(sList));
			return method;
		}
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		if (this.denied) return null;
		else {
			Method m = this.generateControllerMethod();
			return m.generateMethodString();
		}
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		if (this.denied) return null;
		else {
			Method m = this.generateControllerMethod();
			m.setContent(m.generateMethodContentStringWithSerial());
			m.setMethodStatementList(null);
			return m.generateMethodString();
		}
	}

}
