package org.light.core;

import java.io.Serializable;
import java.util.Comparator;

public class CombSerialComparator implements Comparator<Comb>,Serializable{
	private static final long serialVersionUID = -3633372590360798941L;

	@Override
	public int compare(Comb o1, Comb o2) {
		return ((Long)o1.getSerial()).compareTo(o2.getSerial());
	}

}
