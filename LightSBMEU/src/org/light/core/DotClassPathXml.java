package org.light.core;

import java.util.ArrayList;
import java.util.List;

import org.light.domain.ConfigFile;
import org.light.domain.Statement;
import org.light.utils.WriteableUtil;

public class DotClassPathXml extends ConfigFile{
	public DotClassPathXml(){
		super();
		this.topLevel= true;
		this.standardName = ".classpath";
	}

	@Override
	public String generateConfigFileString() throws Exception {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
		sList.add(new Statement(2000L,0,"<classpath>"));
		sList.add(new Statement(3000L,1,"<classpathentry kind=\"src\" path=\"src\"/>"));
		sList.add(new Statement(4000L,1,"<classpathentry kind=\"con\" path=\"org.eclipse.jst.j2ee.internal.web.container\"/>"));
		sList.add(new Statement(5000L,1,"<classpathentry kind=\"con\" path=\"org.eclipse.jst.j2ee.internal.module.container\"/>"));
		sList.add(new Statement(6000L,1,"<classpathentry kind=\"con\" path=\"org.eclipse.jdt.launching.JRE_CONTAINER\"/>"));
		sList.add(new Statement(7000L,1,"<classpathentry kind=\"con\" path=\"org.eclipse.jst.server.core.container/org.eclipse.jst.server.tomcat.runtimeTarget/Apache Tomcat v8.5\"/>"));
		sList.add(new Statement(8000L,1,"<classpathentry kind=\"output\" path=\"build/classes\"/>"));
		sList.add(new Statement(9000L,0,"</classpath>"));
		return WriteableUtil.merge(sList).getContent();
	}
	

}
