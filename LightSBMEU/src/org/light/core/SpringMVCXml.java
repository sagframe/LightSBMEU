package org.light.core;

import java.util.ArrayList;
import java.util.List;

import org.light.domain.ConfigFile;
import org.light.domain.Statement;
import org.light.utils.WriteableUtil;

public class SpringMVCXml extends ConfigFile{
	protected String xmlDefinition = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	protected String packageToken;
	protected String controllerSuffix = "";
	protected String content() { return  "<beans xmlns=\"http://www.springframework.org/schema/beans\"\n"+
			"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:context=\"http://www.springframework.org/schema/context\"\n"+
			"xmlns:mvc=\"http://www.springframework.org/schema/mvc\"\n"+
			"xsi:schemaLocation=\"http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-4.1.xsd\n"+
				"\thttp://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.1.xsd\n"+
				"\thttp://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.1.xsd\">\n\n"+
			"<context:component-scan\n"+
				"\tbase-package=\""+this.getPackageToken()+"."+this.getControllerSuffix()+"\"\n"+
			
				"\tuse-default-filters=\"false\">\n"+
				"\t<context:include-filter type=\"annotation\"\n"+
					"\t\texpression=\"org.springframework.stereotype.Controller\" />\n"+
				"\t<context:include-filter type=\"annotation\"\n"+
					"\t\texpression=\"org.springframework.web.bind.annotation.ControllerAdvice\" />\n"+
			"</context:component-scan>\n\n"+
			getFileUploadSection()+
			"<mvc:annotation-driven>\n"+
				"\t<mvc:message-converters register-defaults=\"true\">\n"+
					"\t\t<bean class=\"org.springframework.http.converter.StringHttpMessageConverter\">\n"+
						"\t\t\t<constructor-arg value=\"UTF-8\" />\n"+
					"\t\t</bean>\n"+
					"\t\t<bean\n"+
						"\t\t\tclass=\"org.springframework.http.converter.json.MappingJackson2HttpMessageConverter\">\n"+
						"\t\t\t<property name=\"prettyPrint\" value=\"true\" />\n"+
						"\t\t\t<property name=\"objectMapper\">\n"+
							 "\t\t\t\t<bean class=\"com.fasterxml.jackson.databind.ObjectMapper\" />\n"+                       
					    "\t\t\t</property>\n"+				
					"\t\t</bean>\n"+
				"\t</mvc:message-converters>\n"+
			"</mvc:annotation-driven>\n"+
			"</beans>\n";
	}
	
	public String getPackageToken() {
		return packageToken;
	}

	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}

	public SpringMVCXml(){
		super();
		this.standardName = "spring-mvc.xml";
	}
	
	@Override 
	public void setStandardName(String standardName){
	}
	
	@Override
	public String generateConfigFileString() {
		StringBuilder sb = new StringBuilder();
		sb.append(xmlDefinition);
		sb.append(content());
		return sb.toString();
	}

	public String getControllerSuffix() {
		return controllerSuffix;
	}

	public void setControllerSuffix(String controllerSuffix) {
		this.controllerSuffix = controllerSuffix;
	}
	
	public String getFileUploadSection() {
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,1,"<!-- 上传文件时需要用到的分解层，默认将编码转为urf-8,设置最大上传容量 ,最大内存-->"));
		sList.add(new Statement(2000L,1,"<bean id=\"multipartResolver\""));
		sList.add(new Statement(3000L,2,"class=\"org.springframework.web.multipart.commons.CommonsMultipartResolver\">"));
		sList.add(new Statement(4000L,2,"<property name=\"defaultEncoding\" value=\"UTF-8\" />"));
		sList.add(new Statement(5000L,2,"<property name=\"maxUploadSize\" value=\"102400000\" />"));
		sList.add(new Statement(6000L,2,"<property name=\"maxInMemorySize\" value=\"4096\" />"));
		sList.add(new Statement(7000L,1,"</bean>"));
		return WriteableUtil.merge(sList).getContent();
	}

}
