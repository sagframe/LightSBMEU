package org.light.wizard;

import java.util.Map;

import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.domain.Project;
import org.light.utils.StringUtil;

public class ProjectWizard {
	
	public static Project createNewProject(Map<String,String> params) {
		Project project = new Project();
		project.setStandardName(StringUtil.nullTrim(params.get("project")));
		project.setPackageToken(StringUtil.nullTrim(params.get("packagetoken")));
		project.setDbPrefix(StringUtil.nullTrim(params.get("dbprefix")));
		project.setDbName(StringUtil.nullTrim(params.get("dbname")));
		project.setDbUsername(StringUtil.nullTrim(params.get("dbusername")));
		project.setDbPassword(StringUtil.nullTrim(params.get("dbpassword")));
		project.setDbType(StringUtil.nullTrim(params.get("dbtype")));
		project.setTechnicalstack(StringUtil.nullTrim(params.get("technicalstack")));
		project.setTitle(StringUtil.nullTrim(params.get("title")));
		project.setSubTitle(StringUtil.nullTrim(params.get("subtitle")));
		project.setFooter(StringUtil.nullTrim(params.get("footer")));
		project.setCrossOrigin(StringUtil.nullTrim(params.get("crossorigin")));
		project.setResolution(StringUtil.nullTrim(params.get("resolution")));
		project.setDomainSuffix(StringUtil.nullTrim(params.get("domainsuffix")));
		project.setDaoSuffix(StringUtil.nullTrim(params.get("daosuffix")));
		project.setDaoimplSuffix(StringUtil.nullTrim(params.get("daoimplsuffix")));
		project.setServiceSuffix(StringUtil.nullTrim(params.get("servicesuffix")));
		project.setServiceimplSuffix(StringUtil.nullTrim(params.get("serviceimplsuffix")));
		project.setControllerSuffix(StringUtil.nullTrim(params.get("controllersuffix")));
		project.setDomainNamingSuffix(StringUtil.nullTrim(params.get("domainnamingsuffix")));
		project.setControllerNamingSuffix(StringUtil.nullTrim(params.get("controllernamingsuffix")));
		project.setLanguage(StringUtil.nullTrim(params.get("language")));
		project.setSchema(StringUtil.nullTrim(params.get("schema")));
		project.setExcelTemplateName(StringUtil.nullTrim(params.get("fileName")));
		project.setFrontBaseApi(StringUtil.nullTrim(params.get("frontbaseapi")));
		return project;
	}
	
	public static Project updateProject(Project project,Map<String,String> params) {
		project.setStandardName(StringUtil.nullTrim(params.get("project")));
		project.setPackageToken(StringUtil.nullTrim(params.get("packagetoken")));
		project.setDbPrefix(StringUtil.nullTrim(params.get("dbprefix")));
		project.setDbName(StringUtil.nullTrim(params.get("dbname")));
		project.setDbUsername(StringUtil.nullTrim(params.get("dbusername")));
		project.setDbPassword(StringUtil.nullTrim(params.get("dbpassword")));
		project.setDbType(StringUtil.nullTrim(params.get("dbtype")));
		project.setTechnicalstack(StringUtil.nullTrim(params.get("technicalstack")));
		project.setTitle(StringUtil.nullTrim(params.get("title")));
		project.setSubTitle(StringUtil.nullTrim(params.get("subtitle")));
		project.setFooter(StringUtil.nullTrim(params.get("footer")));
		project.setCrossOrigin(StringUtil.nullTrim(params.get("crossorigin")));
		project.setResolution(StringUtil.nullTrim(params.get("resolution")));
		project.setDomainSuffix(StringUtil.nullTrim(params.get("domainsuffix")));
		project.setDaoSuffix(StringUtil.nullTrim(params.get("daosuffix")));
		project.setDaoimplSuffix(StringUtil.nullTrim(params.get("daoimplsuffix")));
		project.setServiceSuffix(StringUtil.nullTrim(params.get("servicesuffix")));
		project.setServiceimplSuffix(StringUtil.nullTrim(params.get("serviceimplsuffix")));
		project.setControllerSuffix(StringUtil.nullTrim(params.get("controllersuffix")));
		project.setDomainNamingSuffix(StringUtil.nullTrim(params.get("domainnamingsuffix")));
		project.setControllerNamingSuffix(StringUtil.nullTrim(params.get("controllernamingsuffix")));
		project.setLanguage(StringUtil.nullTrim(params.get("language")));
		project.setSchema(StringUtil.nullTrim(params.get("schema")));
		project.setExcelTemplateName(StringUtil.nullTrim(params.get("fileName")));
		project.setFrontBaseApi(StringUtil.nullTrim(params.get("frontbaseapi")));
		return project;
	}
	
	public static Domain createNewDomain(Map<String,String> params) {
		Domain domain = new Domain();
		domain.setStandardName(StringUtil.nullTrim(params.get("domain")));
		domain.setPlural(StringUtil.nullTrim(params.get("plural")));
		domain.setTablePrefix(StringUtil.nullTrim(params.get("tableprefix")));
		domain.setLabel(StringUtil.nullTrim(params.get("domainlabel")));
		domain.setVerbDeniesStr(StringUtil.nullTrim(params.get("verbdenies")));
		return domain;
	}
	
	public static Domain updateDomain(Domain domain,Map<String,String> params) {
		domain.setStandardName(StringUtil.nullTrim(params.get("domain")));
		domain.setPlural(StringUtil.nullTrim(params.get("plural")));
		domain.setTablePrefix(StringUtil.nullTrim(params.get("tableprefix")));
		domain.setLabel(StringUtil.nullTrim(params.get("domainlabel")));
		domain.setVerbDeniesStr(StringUtil.nullTrim(params.get("verbdenies")));
		return domain;
	}
	
	public static Field createNewField(Map<String,String> params) {
		if ("field".equals(params.get("fieldcatlog"))) {
			Field field = new Field();
			field.setSerial(Long.valueOf(params.get("serial")));
			field.setFieldType(params.get("fieldtype"));
			field.setFieldName(params.get("fieldname"));
			field.setLabel(params.get("fieldlabel"));
			field.setLengthStr(params.get("fieldlength"));
			return field;
		}else if  ("dropdown".equals(params.get("fieldcatlog"))) {
			Dropdown dp = new Dropdown();
			dp.setSerial(Long.valueOf(params.get("serial")));
			dp.setFieldType(params.get("fieldtype"));
			dp.setFieldName(params.get("fieldname"));
			dp.setAliasName(params.get("fieldname"));
			dp.setTargetName(params.get("fieldtype"));
			dp.setLabel(params.get("fieldlabel"));
			dp.setLengthStr(params.get("fieldlength"));
			return dp;
		}else {
			return null;
		}
	}
	
	public static ManyToMany createNewManyToMany(Domain domain,Map<String,String> params) {
		if  ("manytomany".equals(params.get("fieldcatlog"))) {
			ManyToMany mtm = new ManyToMany();
			String capFieldName = StringUtil.capFirst(params.get("fieldname"));
			mtm.setStandardName(capFieldName);
			mtm.setManyToManySalveName(params.get("fieldtype"));
			mtm.setSlaveAliasLabel(params.get("fieldlabel"));
			mtm.setSlaveAlias(capFieldName);
			mtm.setMaster(domain);
			return mtm;
		}else {
			return null;
		}
	}
}
