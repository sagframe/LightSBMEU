package org.light.utils;

import java.util.List;

import org.light.domain.ValidateInfo;
import org.light.exception.ValidateException;

public class ValidateExceptionUtil {
	public ValidateException merge(List<ValidateException> ves) {
		ValidateInfo info0 = new ValidateInfo();
		for (ValidateException ve:ves) {
			info0.addAllCompileErrors(ve.getValidateInfo().getCompileErrors());
			info0.addAllCompileWarnings(ve.getValidateInfo().getCompileWarnings());
		}
		return new ValidateException(info0);
	}
}