package org.light.utils;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.light.domain.Field;
import org.light.domain.FieldSerialComparator;
import org.light.domain.Var;

public class FieldUtil {
	public static String generateRequestGetParameterString(Field field, Var request){
		String type = field.getFieldType();
		switch (type) {
		case "long": return "Long.parseLong(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "int": return "Integer.parseInt(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "float": return "Float.parseFloat(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "double": return "Double.parseDouble(" + request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "String": return request.getVarName() + ".getParameter(\""+field.getFieldName()+"\")";
		case "boolean": return "Boolean.parseBoolean("+request.getVarName()+ ".getParameter(\""+field.getFieldName()+"\"))";
		case "BigDecimal": return "new BigDecimal("+request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "Timestamp": return "new Timestamp("+request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		case "Date": return "new Date("+request.getVarName() + ".getParameter(\""+field.getFieldName()+"\"))";
		default : return null;
		}
	}
	
	public static Set<Field> filterDeniedFields(Set<Field> allFields, Set<Field> deniedFields) {
		Set<Field> results = new TreeSet<Field>(new FieldSerialComparator());
		outer: for (Field f : allFields) {
			for (Field df : deniedFields) {
				String fieldName = f.getFieldName();
				if (fieldName.equals(df.getFieldName())) {
					continue outer;
				}
			}
			results.add(f);
		}
		return results;
	}
	
	public static String genFieldArrayTextQStr(List<Field> fields) {
		StringBuilder sb = new StringBuilder();
		for (Field f:fields) {
			sb.append("\"").append(f.getText()).append("\",");
		}
		String retStr = sb.toString();
		if (retStr.contains(",")) return retStr.substring(0,retStr.length()-1);
		else return retStr;
	}
	
	public static String genFieldArrayLowerFirstNameWithPrefixStr(List<Field> fields, String prefix) {
		StringBuilder sb = new StringBuilder();
		for (Field f:fields) {
			sb.append(prefix).append(f.getLowerFirstFieldName()).append(",");
		}
		String retStr = sb.toString();
		if (retStr.contains(",")) return retStr.substring(0,retStr.length()-1);
		else return retStr;
	}

	public static boolean isNumeric(Field f) {
		boolean retVal = false;
		String fieldType = f.getFieldType();
		if ("int".equals(fieldType)||"Integer".equals(fieldType)||"long".equals(fieldType)||
				"Long".equals(fieldType)||"double".equals(fieldType)||"Double".equals(fieldType)||
				"decimal".equals(fieldType)||"Decimal".equals(fieldType)||"BigDecimal".equals(fieldType)||
				"float".equals(fieldType)||"Float".equals(fieldType)) {
			retVal = true;
		}
		return retVal;
	}
}